<?php

class DNAFactory_BrtExporter_Helper_Decorator_Feed_Order extends Mage_Core_Helper_Abstract implements DNAFactory_ShippingExporter_Helper_Exporter_ShippingInterface
{
    /**
     * @return array
     */
    public function getHeader()
    {
        $header = array(
            'VABCAS', // value cod or null
            'VABCCM', // valore da config associato al cliente
            'VABLNP', // valore da config associato al cliente
            'VABTIC', // Boh, sempre vuoto
            'VABVCA', // currency ex: EUR
            'VABRMN', // increment_id dell'ordine RIFERIMENTO MITTENTE NUMERICO
            'VABRMA', // increment_id dell'ordine RIFERIMENTO MITTENTE ALFABETICO
            'VABCTR', // tariffa
            'VABRSD', // nome del cliente
            'VABCBO', // 1: spedizione normale; 4: cod
            'VABIND', // Indirizzo
            'VABCAD', // cap
            'VABLOD', // città
            'VABPRD', // provincia
            'VABNZD', // nazione
            'VABPKB', // boh, sempre 1
            'VABTSP', // boh, sempre c
            'VABEMD', // email
            'VABCEL', // numero di telefono
            'VABNCL', // boh, sempre 1
            'VABTRC', // numero di telefono
        );

        return $header;
    }

    /**
     * @param $row
     * @return array
     */
    public function getBody($row)
    {
        try {
            $arr = array(
                $this->getVABCAS($row),
                $this->getVABCCM(),
                $this->getVABLNP(),
                $this->getVABTIC(),
                $row['currency'],
                $row['order_id'],
                $row['increment_id'],
                $this->getVABCTR(),
                $row['first_name'].$row['last_name'],
                $this->getVABCBO($row),
                $row['shipping_address'],
                $row['post_code'],
                $row['city'],
                $row['region'],
                $row['country'],
                $this->getVABPKB(),
                $this->getVABTSP(),
                $row['email'],
                $row['telephone'],
                $this->getVABNCL(),
                $row['telephone']
            );
        } catch (Exception $e) {
            $arr = array();
        }

        return $arr;
    }

    /**
     * @return bool
     */
    public function shouldIPrintHeader()
    {
        return true;
    }

    /**
     * @param $orderArray, array di ordine decorato da ShippingExporter
     * @return array|bool, array di righe da importare nel CSV | FALSE
     */
    public function decorateOrder($orderArray)
    {
        if (isset($orderArray)) {
            $body = [];
            foreach ($orderArray as $row) {
                $body[] = $this->getBody($row);
            }
            return $body;
        }
        return false;
    }

    public function getVABCAS($row)
    {
        if ($row['payment_method'] == 'phoenix_cashondelivery')
            return number_format($row['grand_total'], 2, ',', '');

        return '';
    }

    public function getVABCCM()
    {
        return Mage::getStoreConfig('dnafactory_brtexporter/settings/VABCCM');
    }

    public function getVABLNP()
    {
        return Mage::getStoreConfig('dnafactory_brtexporter/settings/VABLNP');
    }

    public function getVABTIC()
    {
        return Mage::getStoreConfig('dnafactory_brtexporter/settings/VABTIC');
    }

    public function getVABCTR()
    {
        return Mage::getStoreConfig('dnafactory_brtexporter/settings/VABCTR');
    }

    public function getVABCBO($row)
    {
        if ($row['payment_method'] == 'cashondelivery')
            return '4';
        return '1';
    }

    public function getVABPKB()
    {
        return Mage::getStoreConfig('dnafactory_brtexporter/settings/VABPKB');
    }

    public function getVABTSP()
    {
        return Mage::getStoreConfig('dnafactory_brtexporter/settings/VABTSP');
    }

    public function getVABNCL()
    {
        return Mage::getStoreConfig('dnafactory_brtexporter/settings/VABNCL');
    }
}