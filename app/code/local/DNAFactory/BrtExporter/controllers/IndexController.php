<?php

class DNAFactory_BrtExporter_IndexController extends Mage_Adminhtml_Controller_Action
{
    public function brtExporterAction()
    {
        if(!$this->getRequest()->isPost() && !$this->getRequest()->isAjax()) {
            return;
        }

        /** @var DNAFactory_DNAtoolkit_Model_Csv $csvExporter */
        $csvExporter = Mage::getModel('dnafactory_dnatoolkit/csv');
        $header = array();
        $body = array();

        /** @var DNAFactory_BrtExporter_Helper_Decorator_Feed_Order $helper */
        $helper = Mage::helper('dnafactory_brtexporter/decorator_feed_order');

        if ($helper->shouldIPrintHeader()) {
            $header = $helper->getHeader();
        }

        $orderIds = $this->getRequest()->getParam('order_ids');

        if (isset($orderIds) && is_array($orderIds)) {

            /** @var DNAFactory_ShippingExporter_Helper_Exporter_Shipping $helperOrder */
            $helperOrder = Mage::helper('shippingexporter/exporter_shipping');
            $orders = $helperOrder->getOrders($orderIds);

            if (isset($orders) && is_array($orders)) {
                $body = $helper->decorateOrder($orders);
            }

            $tmpStorageFolder = Mage::getBaseDir('media') . DS . 'BRT_export';

            if(!file_exists($tmpStorageFolder)) {
                mkdir($tmpStorageFolder, 0777, true);
            }

            $filename = $tmpStorageFolder . DS . 'bartolini-' . date('Y-m-d-H-i-s') . uniqid() . '.csv';

            $csvExporter->export($filename, $header, $body, ';');

            if (file_exists($filename)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($filename).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($filename));
                readfile($filename);
                exit;
            }
        }
    }
}