<?php

class DNAFactory_BrtExporter_Model_Observer
{
    public function addMassAction($observer)
    {
        /** @var Mage_Core_Block_Abstract $block */
        $block = $observer->getEvent()->getBlock();
        $currentClass = get_class($block);
        $currentControllerName = $block->getRequest()->getControllerName();

        if ($currentClass == 'Mage_Adminhtml_Block_Widget_Grid_Massaction' && $currentControllerName == 'sales_order') {

            /**  @var Mage_Adminhtml_Block_Widget_Grid_Massaction $block  */
            $block->addItem('brt_exporter', array(
                'label' => 'Export BRT',
                'url' => Mage::helper('adminhtml')->getUrl('adminhtml/index/brtExporter')
            ));
        }
    }
}