<?php

class DNAFactory_DNAtoolkit_Model_Csv extends Mage_Core_Model_Abstract
{
    public function export($filename, $header, $body, $delimiter = ',', $enclosure = '"')
    {
        if(!($fileHandler = fopen($filename, 'a'))) {
            throw new Exception(Mage::helper('dnafactory_dnatoolkit')->__('Impossibile aprire il file.') . $filename);
        }

        if (!empty($header)) {
            fputcsv($fileHandler, $header, $delimiter, $enclosure);
        }

        foreach ($body as $row) {
            if (!empty($row)) {
                fputcsv($fileHandler, $row, $delimiter, $enclosure);
            }
        }

        return fclose($fileHandler);
    }
}