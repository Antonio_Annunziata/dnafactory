<?php

class DNAFactory_ShippingExporter_Helper_Decorator_Order extends Mage_Core_Helper_Abstract
{
    /**
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function decorateOrder(Mage_Sales_Model_Order $order)
    {
        $array = [
            'order_id' => $order->getId(),
            'increment_id' => $order->getIncrementId(),
            'first_name' => $this->getFirstName($order),
            'last_name' => $this->getLastName($order),
            'shipping_method' => $order->getShippingMethod(),
            'shipping_description' => $order->getShippingDescription(),
            'payment_method' => $this->getPaymentMethod($order),
            'shipping_address' => $this->getShippingAddress($order),
            'post_code' => $this->getPostCode($order),
            'city' => $this->getCity($order),
            'region' => $this->getRegion($order),
            'country' => $this->getCountry($order),
            'email' => $this->getEmail($order),
            'telephone' => $this->getTelephone($order),
            'weight' => $order->getWeight(),
            'grand_total' => $order->getGrandTotal(),
            'subtotal' => $order->getSubtotal(),
            'tax_amount' => $order->getTaxAmount(),
            'discount_amount' => $order->getDiscountAmount(),
            'is_guest' => $order->getCustomerIsGuest(),
            'item_count' => $order->getTotalItemCount(),
            'currency' => $order->getBaseCurrencyCode()
        ];

        return $array;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return null|string
     */
    public function getFirstName(Mage_Sales_Model_Order $order)
    {
        /**
         * @var Mage_Sales_Model_Order_Address $shippingAddress
         */
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress !== false) {
            return $shippingAddress->getFirstname();
        } else {
            return null;
        }
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return null|string
     */
    public function getLastName(Mage_Sales_Model_Order $order)
    {
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress !== false)
            return $shippingAddress->getLastname();
        return null;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
    public function getPaymentMethod(Mage_Sales_Model_Order $order)
    {
        $payment = $order->getPayment();
        $method = $payment->getMethod();
        return $method;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return null|string
     */
    public function getShippingAddress(Mage_Sales_Model_Order $order)
    {
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress !== false) {
            $street = $shippingAddress->getStreet();
            return $street[0] . ' ' . $street[1];
        }
        return null;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return null|string
     */
    public function getPostCode(Mage_Sales_Model_Order $order)
    {
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress !== false)
            return $shippingAddress->getPostcode();
        return null;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return null|string
     */
    public function getCity(Mage_Sales_Model_Order $order)
    {
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress !== false)
            return $shippingAddress->getCity();
        return null;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return mixed|null
     */
    public function getRegion(Mage_Sales_Model_Order $order)
    {
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress !== false)
            return $shippingAddress->getRegionCode();
        return null;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return null|string
     */
    public function getCountry(Mage_Sales_Model_Order $order)
    {
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress !== false)
            return $shippingAddress->getCountryId();
        return null;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return null|string
     */
    public function getEmail(Mage_Sales_Model_Order $order)
    {
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress !== false)
            return $shippingAddress->getEmail();
        return null;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return null|string
     */
    public function getTelephone(Mage_Sales_Model_Order $order)
    {
        $shippingAddress = $order->getShippingAddress();
        if ($shippingAddress !== false)
            return $shippingAddress->getTelephone();
        return null;
    }
}