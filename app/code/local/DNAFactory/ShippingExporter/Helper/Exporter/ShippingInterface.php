<?php

interface DNAFactory_ShippingExporter_Helper_Exporter_ShippingInterface
{
    /**
     * @return array
     */
    public function getHeader();

    /**
     * @return bool
     */
    public function shouldIPrintHeader();

    /**
     * @param $row
     * @return array
     */
    public function getBody($row);

    /**
     * @param $orderArray
     * @return array
     */
    public function decorateOrder($orderArray);
}