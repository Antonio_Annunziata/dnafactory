<?php

/**
 * Class DNAFactory_ShippingExporter_Model
 */

class DNAFactory_ShippingExporter_Helper_Exporter_Shipping extends Mage_Core_Helper_Abstract
{

    /**
     * @param $id, id order magento
     * @return array, order decorated
     */
    public function getOrder($id)
    {
        $order = Mage::getModel('sales/order')->load($id);
        /** @var DNAFactory_ShippingExporter_Helper_Decorator_Order $decoratorOrderHelper */
        $decoratorOrderHelper = Mage::helper('shippingexporter/decorator_order');
        return $decoratorOrderHelper->decorateOrder($order);
    }

    /**
     * @param $ids, ids orders magento
     * @return array, of array: [getOrder(ids[0]), getOrder(ids[1], getOrder(ids[2]), ..., getOrder(ids[n])]
     */
    public function getOrders($ids)
    {
        $orders = array();
        foreach ($ids as $id) {
            $orders[] = $this->getOrder($id);
        }
        return $orders;
    }
}
