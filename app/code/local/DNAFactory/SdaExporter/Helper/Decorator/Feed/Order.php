<?php

class DNAFactory_SdaExporter_Helper_Decorator_Feed_Order extends Mage_Core_Helper_Abstract implements DNAFactory_ShippingExporter_Helper_Exporter_ShippingInterface
{
    /**
     * @return array
     */
    public function getHeader()
    {
        $header = array(
            'RAGSOC', // nome del cliente
            'INDIRIZZO', // Indirizzo
            'CAP', // cap
            'LOC', // città
            'PROVINCIA', // provincia
            'NAZIONE', // nazione
            'EMAIL', // email
            'CELL', // numero di telefono
            'TEL', // numero di telefono
            'RIFINT', // increment_id dell'ordine
            'PESO',
            'CONTRASSEGNO'
        );

        return $header;
    }

    /**
     * @param $row
     * @return array
     */
    public function getBody($row)
    {
        try {
            $arr = array(
                $row['first_name'].$row['last_name'],
                $row['shipping_address'],
                $row['post_code'],
                $row['city'],
                $row['region'],
                $row['country'],
                $row['email'],
                $row['telephone'],
                $row['telephone'],
                $row['increment_id'],
                $row['weight'],
                $this->getCod($row)
            );
        } catch (Exception $e) {
            $arr = array();
        }

        return $arr;
    }

    /**
     * @return bool
     */
    public function shouldIPrintHeader()
    {
        return true;
    }

    /**
     * @param $orderArray, array di ordine decorato da ShippingExporter
     * @return array|bool, array di righe da importare nel CSV | FALSE
     */
    public function decorateOrder($orderArray)
    {
        if (isset($orderArray)) {
            $body = [];
            foreach ($orderArray as $row) {
                $body[] = $this->getBody($row);
            }
            return $body;
        }
        return false;
    }

    public function getCod($row)
    {
        if ($row['payment_method'] == 'phoenix_cashondelivery')
            return number_format($row['grand_total'], 2, ',', '');

        return '';
    }
}